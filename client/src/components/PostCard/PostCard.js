import React, { useState } from "react";
import { Card, CardBody, CardHeader, CardImg, Col, UncontrolledCollapse } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import "./style.css";
import { dateFormate } from "../../utils/dateFormate";

const PostCard = ({ post, handleFavourite, handleLike }) => {
  const [commentToggle, setcommentToggle] = useState("View");
  const [likes, setlikes] = useState(post.isLike);
  const [favourite, setFavourite] = useState(post.isFavourite);
  let tags = post.tags ? post.tags : [];
  let comments = post.comments ? post.comments : [];
  const onCommentClicked = () => {
    commentToggle === "View" ? setcommentToggle("Hide") : setcommentToggle("View");
  };
  const toggleLike = (postId) => {
    handleLike(postId);
    setlikes(!likes);
  };
  const toggleFavourite = (postId) => {
    handleFavourite(postId);
    setFavourite(!favourite);
  };

  return (
    <Col sm={10} md={7}>
      <Card>
        <CardHeader style={{ border: 0, backgroundColor: "#fff", borderRadius: 0, display: "flex" }}>
          <img src={`${post?.user?.img}`} className="profileImg" alt="profile" />
          <div className="nameContainer">
            <h6 className="userName">{post?.user?.name}</h6>
            <p>{dateFormate(post.createdAt)}</p>
          </div>
        </CardHeader>
        <div className="postImg ">
          <CardImg
            className="p-0 m-0"
            style={{ border: 0, backgroundColor: "#fff", borderRadius: 0, objectFit: "contain" }}
            width="100%"
            src={`${post.img}`}
            alt="post image"
          />
          <div className="shade"></div>
          <div className="imgText ">
            <h6 className="cardTitle">{post.title}</h6>
            <h5 className="cardPrice">
              {post.price}
              {"  "}
              {post.currancy}
            </h5>

            <FontAwesomeIcon
              className={`favIcon btn p-0 ${favourite ? "text-danger" : "text-dark"}`}
              onClick={() => {
                toggleFavourite(post._id);
              }}
              icon={faHeart}
            />
          </div>
        </div>
        <CardBody>
          <div
            className={`likes btn p-0 ${likes ? "text-primary" : "text-secondary"}`}
            onClick={() => {
              toggleLike(post._id);
            }}
          >
            <FontAwesomeIcon className="likeIcon" icon={faHeart} />
            <h6 className="likeText">{post.likesCount}</h6>
          </div>
          <p className="discription">{post.description}</p>
          <p className="tages">
            {tags.length > 0 ? (
              tags.map((tag, index) => (
                <a key={index} style={{ color: "#297eda" }} href="#">
                  #{tag} {"     "}
                </a>
              ))
            ) : (
              <></>
            )}
          </p>
          <span id={`post${post._id}`} onClick={onCommentClicked} className="btn commentsBtn">
            {commentToggle} {comments.length} comment
          </span>
          <UncontrolledCollapse toggler={`#post${post._id}`}>
            {comments.length > 0 ? (
              comments.map((comment) => (
                <div key={comment._id} className="m-2 commentsContainer">
                  <img
                    src={`${comment.userId.img}`}
                    width="25"
                    height="25"
                    className="commentImg"
                    alt="user profile "
                  />
                  <div>
                    <h6 className="commentName">
                      {comment.userId.name}
                      <span className="commentTime ml-2">{dateFormate(comment.createdAt)}</span>
                    </h6>
                    <span className="commentTxt">{comment.message}</span>
                  </div>
                </div>
              ))
            ) : (
              <></>
            )}
          </UncontrolledCollapse>
          <hr className="mb-0" />
        </CardBody>
      </Card>
    </Col>
  );
};

export { PostCard };
