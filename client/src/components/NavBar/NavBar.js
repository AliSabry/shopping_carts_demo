import React from "react";
import { Tabs, AppBar, Tab, Paper } from "@material-ui/core";
import { Home, Favorite } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import "./style.css";
const NavBar = () => {
  const useStyles = makeStyles({
    root: {
      flexGrow: 1,
      marginBottom: "50px",
    },
    appBar: {
      "@media only screen and (max-width: 800px)": {
        top: "auto",
        bottom: 0,
      },
    },
  });
  const classes = useStyles();
  const [value, setValue] = React.useState(1);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Paper square className={classes.root}>
      <AppBar className={classes.appBar} position="fixed">
        <Tabs
          className="tabs"
          TabIndicatorProps={{ style: { backgroundColor: "rgb(228, 181, 228)" } }}
          value={value}
          variant="fullWidth"
          centered
          onChange={handleChange}
        >
          <Tab className="tab-item" value={1} icon={<Home />} component={Link} to="/" />
          <Tab className="tab-item" value={2} icon={<Favorite />} component={Link} to="/Fav" />
        </Tabs>
      </AppBar>
    </Paper>
  );
};

export { NavBar };
