// return the token from the session storage
export const getUserId = () => {
  return localStorage.getItem("USER_ID") || null;
};

// remove the token and user from the session storage
export const removeUserId = () => {
  localStorage.removeItem("USER_ID");
};

// set the token and user from the session storage
export const setUserId = (id) => {
  localStorage.setItem("USER_ID", id);
};
