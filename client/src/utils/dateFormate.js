import moment from "moment";
export const dateFormate = (timeStamp) => {
  return moment(timeStamp).format("DD/MM/YYYY - h:m a").toLocaleString();
};
