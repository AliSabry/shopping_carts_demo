import React, { useEffect, useState } from "react";
import { Alert, Container, Row } from "reactstrap";
import { apiAddFavPost } from "../../api/favourite";
import { apiAddLikePost } from "../../api/likes";
import { apiFetchPosts } from "../../api/post";
import { PostCard } from "../../components/PostCard/PostCard";
import { getUserId } from "../../utils";

const HomePage = () => {
  const [posts, setPosts] = useState([]);
  const [isFavourite, setIsFavourite] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  useEffect(() => {
    handelFetchingPosts(getUserId());
  }, []);
  const handelFetchingPosts = (userId) => {
    apiFetchPosts(userId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          setPosts(data.data);
        } else {
          setPosts([]);
        }
      })
      .catch((err) => {
        console.log(err);
        if (err.response) {
          let {
            data: { errors: errors },
          } = err.response;
          setErrorMsg(errors[0]);
        } else {
          setErrorMsg("server error");
        }
      });
  };

  const handleAddFavPost = (postId) => {
    apiAddFavPost(getUserId(), postId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          setIsFavourite(!isFavourite);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleAddLikePost = (postId) => {
    apiAddLikePost(getUserId(), postId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          handelFetchingPosts(getUserId());
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <Container className="themed-container mt-2 p-2">
        {errorMsg ? (
          <Alert color="danger" className="text-center">
            {errorMsg}
          </Alert>
        ) : null}
        <Row className="p-0 m-0 justify-content-center">
          {posts.length > 0 ? (
            posts.map((item) => (
              <PostCard
                key={item._id}
                post={item}
                handleFavourite={handleAddFavPost}
                handleLike={handleAddLikePost}
              />
            ))
          ) : (
            <h5 className="text-muted">
              No Posts Added <i className="far fa-smile-beam"></i>
            </h5>
          )}
        </Row>
      </Container>
    </>
  );
};

export { HomePage };
