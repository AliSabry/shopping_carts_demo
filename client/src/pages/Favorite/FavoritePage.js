import React, { useEffect, useState } from "react";
import { Alert, Container, Row } from "reactstrap";
import { apiFetchFavPosts, apiAddFavPost } from "../../api/favourite";
import { apiAddLikePost } from "../../api/likes";
import { PostCard } from "../../components/PostCard/PostCard";
import { getUserId } from "../../utils";

const FavoritePage = () => {
  const [posts, setPosts] = useState([]);
  const [errorMsg, setErrorMsg] = useState(null);
  useEffect(() => {
    handelFetchingPosts(getUserId());
  }, []);

  const handleAddFavPost = (postId) => {
    apiAddFavPost(getUserId(), postId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          handelFetchingPosts(getUserId());
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleAddLikePost = (postId) => {
    apiAddLikePost(getUserId(), postId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          handelFetchingPosts(getUserId());
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handelFetchingPosts = (userId) => {
    apiFetchFavPosts(userId)
      .then((res) => {
        const { data } = res;
        if (data.success) {
          setPosts(data.data);
        } else {
          setPosts([]);
        }
      })
      .catch((err) => {
        if (err.response) {
          let {
            data: { errors: errors },
          } = err.response;
          setErrorMsg(errors[0]);
        } else {
          setErrorMsg("server error");
        }
      });
  };

  return (
    <>
      <Container className="themed-container mt-2 p-2">
        {errorMsg ? (
          <Alert color="danger" className="text-center">
            {errorMsg}
          </Alert>
        ) : null}
        <Row className="p-0 m-0 justify-content-center">
          {posts.length > 0 ? (
            posts.map((item) => (
              <PostCard
                key={item._id}
                post={item}
                handleFavourite={handleAddFavPost}
                handleLike={handleAddLikePost}
              />
            ))
          ) : (
            <h5 className="text-muted mt-2">
              No favourite posts <i className="far fa-smile-beam"></i>
            </h5>
          )}
        </Row>
      </Container>
    </>
  );
};

export { FavoritePage };
