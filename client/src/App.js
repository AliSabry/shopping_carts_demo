import React from "react";
import { Switch, Route } from "react-router-dom";
import { NavBar } from "./components/NavBar/NavBar";
import { FavoritePage } from "./pages/Favorite/FavoritePage";
import { HomePage } from "./pages/Home/HomePage";
import { setUserId } from "./utils";

const App = () => {
  // save user id in local storage
  setUserId("60c735acbf0fa05e821051a2");
  
  return (
    <React.Fragment>
      <NavBar />
      <Switch>
        {/* Public Routes */}
        <Route path="/" component={HomePage} exact />
        <Route path="/Fav" component={FavoritePage} exact />
      </Switch>
    </React.Fragment>
  );
};

export { App };
