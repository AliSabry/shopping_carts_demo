import axios from "axios";
import { API_CONST } from "../utils/index";

export const apiAddLikePost = (userId, postId) => {
  return axios.post(`${API_CONST}/like`, { userId, postId });
};
