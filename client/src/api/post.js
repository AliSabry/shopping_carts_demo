import axios from "axios";
import { API_CONST } from "../utils/index";

export const apiFetchPosts = (userId) => {
  return axios.get(`${API_CONST}/posts`, { params: { userId } });
};
