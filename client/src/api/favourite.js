import axios from "axios";
import { API_CONST } from "../utils/index";

export const apiFetchFavPosts = (userId) => {
  return axios.get(`${API_CONST}/favourite`, { params: { userId } });
};

export const apiAddFavPost = (userId, postId) => {
  return axios.post(`${API_CONST}/favourite`, { userId, postId });
};
