"use strict";
const path = require("path");
const winston = require("winston");
const { combine, timestamp, colorize, printf } = winston.format;

// define the custom settings for each transport (file, console)
const options = {
  file: {
    level: "error",
    filename: `${path.join(__dirname, "../logs/server.log")}`,
    handleExceptions: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: true,
  },
  console: {
    level: "debug",
    handleExceptions: true,
    colorize: true,
  },
  server: {
    level: "server",
    handleExceptions: true,
    colorize: true,
  },
};

const customFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp}  | ${level}  |  ${message}`;
});

const logger = winston.createLogger({
  level: "info",
  format: combine(
    colorize({ all: true }),
    timestamp({ format: "DD-MM-YYYY HH:mm:ss" }),
    customFormat
  ),
  transports: [
    // Write all logs with level `error` and below to `server.log`
    new winston.transports.File(options.file),
  ],
  exitOnError: false,
});
// create a stream object with a 'write' , function that will be used by `morgan`
logger.stream = {
  write: function (message, encoding) {
    // use the 'info' log level  the output will be by both transports (file and console)
    logger.info(message);
  },
};
// If not in production then log to the `console`
if (process.env.NODE_ENV !== "production") {
  logger.add(new winston.transports.Console(options.console));
}

module.exports = logger;
