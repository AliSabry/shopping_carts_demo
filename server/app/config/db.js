"use strict";
const mongoose = require("mongoose");
const config = require("./config.json");
const logger = require("./logger");
const DB_URL = process.env.DB_URL || config.DB_URL;

module.exports = () => {
  // connect to mongo DB
  mongoose
    .connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(() => console.info("Connected to DB successfully"))
    .catch((err) => logger.error(`DB connection Failed ${err}`));
};
