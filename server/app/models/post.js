const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostSchema = new Schema({
  title: {
    type: String,
  },
  price: {
    type: Number,
  },
  currancy: {
    type: String,
  },
  description: {
    type: String,
  },
  tags: { type: Array },
  img: {
    type: String,
  },
  createdAt: {
    type: Number,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const PostModel = mongoose.model("Post", PostSchema);
module.exports = PostModel;
