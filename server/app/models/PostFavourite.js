const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostFavouriteSchema = new Schema({
  postId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post",
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const PostFavouriteModel = mongoose.model("PostFavourite", PostFavouriteSchema);
module.exports = PostFavouriteModel;
