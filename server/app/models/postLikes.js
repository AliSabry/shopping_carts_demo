const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostLikeSchema = new Schema({
  postId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post",
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const PostLikeModel = mongoose.model("PostLike", PostLikeSchema);
module.exports = PostLikeModel;
