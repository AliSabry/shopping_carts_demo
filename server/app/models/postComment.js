const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostCommentSchema = new Schema({
  postId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post",
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  message: {
    type: String,
  },
  createdAt: {
    type: Number,
  },
});

const PostCommentModel = mongoose.model("PostComment", PostCommentSchema);
module.exports = PostCommentModel;
