"use strict";

const logger = require("../config/logger");
const { API_CONST } = require("../utils/constents");
module.exports = (app) => {
  app.use(`${API_CONST}/posts`, require("./posts.routes"));
  app.use(`${API_CONST}/favourite`, require("./favouite.routes"));
  app.use(`${API_CONST}/like`, require("./likes.routes"));

};
