const express = require("express");
const router = express.Router();
const favController = require("../controllers/favourite.controller");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");
// get user posts list API
router.get("/", async (req, res, next) => {
  let { userId } = req.query;
  if (!userId) {
    res.status(CODE.BAD_REQUEST).send(apiResponse([], ["UserId and postId are required"]));
  }
  let result = await favController.getFavPosts(userId);
  res.status(result.code).send(result.response);
});

router.post("/", async (req, res, next) => {
  let { userId, postId } = req.body;
  if (!userId || !postId) {
    res.status(CODE.BAD_REQUEST).send(apiResponse([], ["UserId and postId are required"]));
  }
  let result = await favController.addFavPosts(userId, postId);
  res.status(result.code).send(result.response);
});
module.exports = router;
