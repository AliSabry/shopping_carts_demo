const express = require("express");
const router = express.Router();
const likesController = require("../controllers/likes.controller");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");

// add or remove user posts like list API
router.post("/", async (req, res, next) => {
  let { userId, postId } = req.body;
  if (!userId || !postId) {
    res.status(CODE.BAD_REQUEST).send(apiResponse([], ["UserId and postId are required"]));
  }
  let result = await likesController.addLikePost(userId, postId);
  res.status(result.code).send(result.response);
});
module.exports = router;
