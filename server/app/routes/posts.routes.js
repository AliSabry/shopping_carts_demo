const express = require("express");
const router = express.Router();
const postController = require("../controllers/post.controller");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");
// get user posts list API
router.get("/", async (req, res, next) => {
  let { userId } = req.query;
  if (!userId) {
    res.status(CODE.BAD_REQUEST).send(apiResponse([], ["User id is required"]));
  }
  let result = await postController.getPosts(userId);
  res.status(result.code).send(result.response);
});

module.exports = router;
