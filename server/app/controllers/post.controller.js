const PostModel = require("../models/post");
const UserModel = require("../models/user");
const PostCommentsModel = require("../models/postComment");
const PostLikesModel = require("../models/postLikes");
const PostFavouriteModel = require("../models/PostFavourite");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");

class Post {
  async getPosts(userId, postIds) {
    let query = {};
    if (postIds) {
      query = { _id: { $in: postIds } };
    }
    return PostModel.find(query)
      .sort("-createdAt")
      .populate({ path: "userId", model: UserModel })
      .then(async (posts) => {
        // get post comments and no.of Likes
        let response = [];
        if (posts.length > 0) {
          for (let i = 0; i < posts.length; i++) {
            let comments = await this.getPostComment(posts[i]._id);
            let likesCount = await this.getPostLikesCount(posts[i]._id);
            // check if user like the post
            let isLike = await this.checkUserLikeById(userId, posts[i]._id);
            // check if user add post to favourite
            let isFavourite = await this.checkIsPostFavourite(userId, posts[i]._id);
            let post = {
              _id: posts[i]._id,
              tags: posts[i].tags,
              title: posts[i].title,
              img: posts[i].img,
              price: posts[i].price,
              currancy: posts[i].currancy,
              description: posts[i].description,
              createdAt: posts[i].createdAt,
              user: {
                _id: posts[i].userId._id,
                name: posts[i].userId.name,
                img: posts[i].userId.img,
              },
              comments: comments,
              likesCount,
              isLike,
              isFavourite,
            };
            response.push(post);
          }
        }
        return apiResponse(response, [], CODE.OK);
      })
      .catch((err) => {
        return apiResponse([], null, CODE.OK);
      });
  }

  async getPostById(userId, postId) {}

  async getPostComment(postId) {
    return await PostCommentsModel.find({ postId }).sort("-createdAt").populate({
      path: "userId",
      model: UserModel,
    });
  }

  async checkUserLikeById(userId, postId) {
    let likes = await PostLikesModel.findOne({ postId, userId });
    // let likesUsersIds = likes.map((like) => like.userId.toString());
    // // check if user like the post
    return likes ? true : false;
  }

  async getPostLikesCount(postId) {
    let likes = await PostLikesModel.find({ postId });
    return likes.length;
  }
  async checkIsPostFavourite(userId, postId) {
    let favourite = await PostFavouriteModel.findOne({ postId, userId });
    return favourite ? true : false;
  }
}

module.exports = new Post();
