const PostlikeModel = require("../models/postLikes");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");

class Likes {
  async addLikePost(userId, postId) {
    // check if post is liked
    // if not add post  like
    const likePostsExist = await PostlikeModel.findOne({ userId, postId });
    if (likePostsExist) {
      // remove post from like
      return PostlikeModel.findByIdAndDelete(likePostsExist._id)
        .then((like) => {
          return apiResponse([{ msg: "like removed from post successfully" }], [], CODE.OK);
        })
        .catch((err) => {
          return apiResponse([], ["Server Error"], CODE.SERVER_ERROR);
        });
    }
    // add like to like
    return new PostlikeModel({ userId, postId })
      .save()
      .then((like) => {
        return apiResponse([{ msg: "like added to post successfully" }], [], CODE.OK);
      })
      .catch((err) => {
        return apiResponse([], ["Server Error"], CODE.SERVER_ERROR);
      });
  }
}

module.exports = new Likes();
