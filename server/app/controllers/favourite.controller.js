const PostFavouriteModel = require("../models/PostFavourite");
const apiResponse = require("../utils/apiResponse");
const CODE = require("../utils/statusCode");
const postController = require("./post.controller");

class Favourite {
  async getFavPosts(userId) {
    const favPosts = await PostFavouriteModel.find({ userId });
    const postsIds = favPosts.map((post) => post.postId);
    const postsDetails = await postController.getPosts(userId, postsIds);

    // get posts details
    return postsDetails;
  }

  async addFavPosts(userId, postId) {
    // check if post is fav
    // if not add post to fav
    const favPostsExist = await PostFavouriteModel.findOne({ userId, postId });
    if (favPostsExist) {
      // remove post from fav
      return PostFavouriteModel.findByIdAndDelete(favPostsExist._id)
        .then((post) => {
          return apiResponse([{ msg: "post removed from fav successfully" }], [], CODE.OK);
        })
        .catch((err) => {
          return apiResponse([], ["Server Error"], CODE.SERVER_ERROR);
        });
    }
    // add post to fav
    return new PostFavouriteModel({ userId, postId })
      .save()
      .then((post) => {
        return apiResponse([{ msg: "post added to fav successfully" }], [], CODE.OK);
      })
      .catch((err) => {
        return apiResponse([], ["Server Error"], CODE.SERVER_ERROR);
      });
  }
}

module.exports = new Favourite();
