"use strict";
const logger = require("../config/logger");
const apiResponse = require("./apiResponse");
const CODE = require("./statusCode");

module.exports = (app) => {
  // handle page not found
  app.use((req, res, next) => {
    res.status(CODE.NOT_FOUND).render("notFound.ejs");
  });
  // general error handler
  app.use((err, req, res, next) => {
    logger.error(err.stack);
    const code = err.code || CODE.SERVER_ERROR;
    let response = apiResponse([], err.message || " Internal server error");
    res.status(code).send(response.response);
  });
};
