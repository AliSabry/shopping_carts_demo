module.exports = (data, error, code) => {
  return {
    code,
    response: {
      success: error.length === 0,
      data: data ? data : [],
      errors: error ? error : [],
    },
  };
};
