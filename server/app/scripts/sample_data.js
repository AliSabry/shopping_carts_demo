"use strict";
const mongoose = require("mongoose");
const config = require("../config/config.json");
const logger = require("../config/logger");
const PostModel = require("../models/post");
const PostCommentModel = require("../models/postComment");
const PostLikeModel = require("../models/postLikes");
const DB_URL = process.env.DB_URL || config.DB_URL;
const UserModel = require("../models/user");
const usersData = require("./userData");
// connect DB
mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.info("Connected to DB successfully");
    // Save users

    UserModel.insertMany(usersData).then((users) => {
      console.log("User saved successfully");
      // save posts
      let posts = [];
      for (let k = 0; k < users.length; k++) {
        for (let i = k * 2; i < k * 2 + 2; i++) {
          let post = {
            title: `product ${i + 1} title name `,
            img: `/uploads/products/post${i + 1}.jpg`,
            price: 50 + getRandom(1000, 50),
            currancy:'AED',
            description: "Excepteur sint occaecat deserunt mollit anim id est laborum.",
            tags: ["tag1", "tag2", "tag3"],
            createdAt: Date.now() - getRandom(10000000, 1000),
            userId: users[k]._id,
          };
          posts.push(post);
        }
      }
      PostModel.insertMany(posts).then(async (posts) => {
        console.log("posts saved success");
        let comments = [];
        let users = await UserModel.find();
        for (let y = 0; y < posts.length; y++) {
          for (let z = 0; z < users.length; z++) {
            let comment = {
              postId: posts[y]._id,
              userId: users[z]._id,
              message: "Lorem ispansum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
              createdAt: Date.now() + getRandom(10000000, 1000),
            };
            comments.push(comment);
          }
        }
        // ***************
        // save post comments
        PostCommentModel.insertMany(comments).then((comments) => {
          console.log("comments saved");
        });
        // ***************
        // save post likes
        let likes = [];
        for (let k = 1; k < users.length; k++) {
          for (let i = 1; i < posts.length; i += 2) {
            // console.log(i);
            let like = {
              postId: posts[i]._id,
              userId: users[k]._id,
            };
            likes.push(like);
          }
        }
        PostLikeModel.insertMany(likes).then((likes) => {
          console.log("likes saved");
        });
      });
    });
  })
  .catch((err) => logger.error(`DB connection Failed ${err}`));

const getRandom = (max, min) => {
  return Math.floor(Math.random() * (max - min) + min);
};
