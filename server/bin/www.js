"use strict";
/**
 * Module dependancies
 */
require("dotenv").config();
const http = require("http");
const app = require("../server");
const config = require("../app/config/config.json");

// set variabls
const PORT = process.env.PORT || config.PORT;
app.set("port", PORT);

// Create HTTP server using app
http.createServer(app).listen(PORT, () => console.info(`Server start at port ${PORT}`));

// connect DB
require("../app/config/db")();
