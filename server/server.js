"use strict";
/**
 * Module dependancies
 */
const express = require("express");
const cors = require("cors");
const compression = require("compression");
const morgan = require("morgan");
const path = require("path");
const logger = require("./app/config/logger");
const app = express();
/**
 * middlewares
 */
app.use(morgan("dev", { stream: logger.stream }));
app.use(compression());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// set public folder
app.use(express.static(path.join(__dirname, "app", "public")));
// view engine setup => set to ejs
app.set("views", path.join(__dirname, "app", "views"));
app.set("view engine", "ejs");
// render react app in production mode
if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "build")));
  app.get("*", (req, res, next) => {
    if (req.url.includes("/api")) {
      next();
    } else {
      res.sendFile(path.join(__dirname, "build", "index.html"));
    }
  });
}
// Routes
require("./app/routes/index.routes")(app);
// Error handler
require("./app/utils/errorHandler")(app);

module.exports = app;
