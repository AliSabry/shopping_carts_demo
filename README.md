# Shopping Cart Demo

### NodeJs && React

- clone https://gitlab.com/AliSabry/shopping_carts_demo.git

#### Node Server

- PORT : 5000
- cd ./shopping_carts_demo/server
- run development => npm run dev
- run product => npm start

#### React Server

- PORT : 3000
- cd ./shopping_carts_demo/client
- run => npm start
